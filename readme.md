# test

php のコンテナ作ってみるテスト

```sh
# ローカルでビルド
docker build -t lightsail-container-20230413 .
# 走らせてみる ( localhost:8080 でアクセスできる )
docker run -p 8080:80 -it --rm lightsail-container-20230413
```
