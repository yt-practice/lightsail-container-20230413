FROM php:8-apache

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite

COPY . /var/www/
RUN chown -R www-data:www-data /var/www

WORKDIR /var/www
EXPOSE 80

CMD ["sh", "start-apache.sh"]
